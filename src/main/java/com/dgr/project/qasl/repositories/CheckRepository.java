package com.dgr.project.qasl.repositories;

import com.dgr.project.qasl.model.Check;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by user on 20.09.2018.
 */
// 
@Repository
public interface CheckRepository extends JpaRepository<Check, Long> {

    List<Check> findByOperationDateBetween(LocalDateTime startDate, LocalDateTime endDate);
}