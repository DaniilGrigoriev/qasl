package com.dgr.project.qasl.controller;

import com.dgr.project.qasl.model.Check;
import com.dgr.project.qasl.services.CheckService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by user on 20.09.2018.
 */
@RestController
@RequestMapping("/check")
public class CheckController {

    private final CheckService checkService;

    public CheckController(CheckService checkRepository) {
        this.checkService = checkRepository;
    }

    // В контроллерах используется внутренния модель до тех пор,
    // пока она совпадет с моделью API контроллеров. Когда она начнет меняться
    // их нужно разделить, чтобы внутрению модель не показывать наружу.
    // Это позволит выполнять рефакторинг внутрений модели без изменения контракта приложения
    @PostMapping
    public Check saveCheck(@Valid @RequestBody Check check) {
        checkService.save(check);
        return check;
    }

    @RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.GET)
    public Check getById(@PathVariable("id") Long id) {
        return checkService.findOne(id);
    }
}
