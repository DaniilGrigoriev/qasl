package com.dgr.project.qasl.controller;

import com.dgr.project.qasl.services.api.DateRange;
import com.dgr.project.qasl.services.StatService;
import com.dgr.project.qasl.model.stat.CheckStat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by user on 21.09.2018.
 */
@RestController
@RequestMapping("/stat/checkByDateRange")
public class StatController {
    private final StatService statService;

    public StatController(StatService statService) {
        this.statService = statService;
    }

    @PostMapping
    public CheckStat checkStatByDateRange(@Valid @RequestBody DateRange dateRange) {
        return statService.findByDateRange(dateRange);
    }
}

