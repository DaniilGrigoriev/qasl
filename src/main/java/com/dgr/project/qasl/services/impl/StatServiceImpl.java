package com.dgr.project.qasl.services.impl;

import com.dgr.project.qasl.services.api.DateRange;
import com.dgr.project.qasl.model.Check;
import com.dgr.project.qasl.services.StatService;
import com.dgr.project.qasl.model.stat.CheckStat;
import com.dgr.project.qasl.repositories.CheckRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Created by user on 21.09.2018.
 */

@Component
public class StatServiceImpl implements StatService {
    private static Logger log = LoggerFactory.getLogger(StatServiceImpl.class);
    private final CheckRepository checkRepository;

    public StatServiceImpl(CheckRepository checkRepository) {
        this.checkRepository = checkRepository;
    }

    //TODO UNTI Test
    @Override
    public CheckStat findByDateRange(DateRange dateRange) {
        Objects.requireNonNull(dateRange);
        List<Check> checkOfList = checkRepository.findByOperationDateBetween(dateRange.getStartDate(), dateRange.getEndDate());

        long checkCount = checkOfList.stream().count();
        Optional<BigDecimal> sumAmountOptional = checkOfList.stream()
                .map(Check::getAmount)
                .reduce((x1, x2) -> x1.add(x2));

        CheckStat checkStat = new CheckStat();
        checkStat.setCheckCount(checkCount);

        if (sumAmountOptional.isPresent()){
            BigDecimal sumAmount = sumAmountOptional.get();
            BigDecimal avgAmount = sumAmount.divide(
                    BigDecimal.valueOf(checkCount), 2, 2);

            avgAmount.setScale(2, 2);
            sumAmount.setScale(2, 2);

            checkStat.setAvgAmount(avgAmount);
            checkStat.setSumAmount(sumAmount);
        } else {
            checkStat.setAvgAmount(BigDecimal.ZERO);
            checkStat.setSumAmount(BigDecimal.ZERO);
        }
        log.debug("result of findByDateRange with {} is {}", dateRange, checkStat);
        return checkStat;
    }
}

