package com.dgr.project.qasl.services;

import com.dgr.project.qasl.model.stat.CheckStat;
import com.dgr.project.qasl.services.api.DateRange;

/**
 * Created by user on 21.09.2018.
 */
public interface StatService {
    CheckStat findByDateRange(DateRange dateRange);
}
