package com.dgr.project.qasl.services.api;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by user on 21.09.2018.
 */
public class DateRange {
    @NotNull
    private LocalDateTime startDate;
    @NotNull
    private LocalDateTime endDate;

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }
}
