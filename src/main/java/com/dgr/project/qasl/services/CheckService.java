package com.dgr.project.qasl.services;

import com.dgr.project.qasl.model.Check;
import org.springframework.stereotype.Component;

/**
 * Created by user on 20.09.2018.
 */
@Component
public interface CheckService {
    Check findOne(Long aLong);

    Check save(Check check);
}