package com.dgr.project.qasl.services.impl;

import com.dgr.project.qasl.model.Check;
import com.dgr.project.qasl.services.CheckService;
import com.dgr.project.qasl.repositories.CheckRepository;
import org.springframework.stereotype.Component;

/**
 * Created by user on 20.09.2018.
 */
@Component
public class CheckServiceImpl implements CheckService{
    private final CheckRepository checkRepository;

    public CheckServiceImpl(CheckRepository checkRepository) {
        this.checkRepository = checkRepository;
    }

    @Override
    public Check findOne(Long aLong) {
        return checkRepository.findOne(aLong);
    }

    @Override
    public Check save(Check check) {
        return checkRepository.save(check);
    }
}
