package com.dgr.project.qasl.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by user on 20.09.2018.
 */

// Возможно имя класса не очень удачное и лучше было испльзовать Receipt
@Entity
@Table(name = "CHECKS")
public class Check {
    @Id
    @Column(name = "CHECK_ID")
    @NotNull
    private Long checkId;

    @Column(name = "AMOUNT")
    @NotNull
    private BigDecimal amount;

    @Column(name = "OPERATION_DATE")
    @NotNull
    private LocalDateTime operationDate;

    public Long getCheckId() {
        return checkId;
    }

    public void setCheckId(Long checkId) {
        this.checkId = checkId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(LocalDateTime operationDate) {
        this.operationDate = operationDate;
    }

    @Override
    public String toString() {
        return "Check{" +
                "checkId=" + checkId +
                ", amount=" + amount +
                ", operationDate=" + operationDate +
                '}';
    }
}
