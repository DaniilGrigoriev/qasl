package com.dgr.project.qasl.model.stat;

import java.math.BigDecimal;

/**
 * Created by user on 21.09.2018.
 */
public class CheckStat {
    private Long checkCount;
    private BigDecimal avgAmount;
    private BigDecimal sumAmount;

    public Long getCheckCount() {
        return checkCount;
    }

    public void setCheckCount(Long checkCount) {
        this.checkCount = checkCount;
    }

    public BigDecimal getAvgAmount() {
        return avgAmount;
    }

    public void setAvgAmount(BigDecimal avgAmount) {
        this.avgAmount = avgAmount;
    }

    public BigDecimal getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(BigDecimal sumAmount) {
        this.sumAmount = sumAmount;
    }

    @Override
    public String toString() {
        return "CheckStat{" +
                "checkCount=" + checkCount +
                ", avgAmount=" + avgAmount +
                ", sumAmount=" + sumAmount +
                '}';
    }
}
