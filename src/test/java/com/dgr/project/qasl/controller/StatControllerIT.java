package com.dgr.project.qasl.controller;

import com.dgr.project.qasl.services.api.DateRange;
import com.dgr.project.qasl.model.Check;
import com.dgr.project.qasl.model.stat.CheckStat;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by user on 21.09.2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatControllerIT {
    @Autowired
    private TestRestTemplate restTemplate;

   private List<Check> testData;
   private DateRange dateRange;
   private CheckStat checkStatExpect;

    public TestRestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(TestRestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Before
    public void before() {
        LocalDateTime startPeriod = LocalDateTime.of(2018, 9, 20, 0, 0);
        LocalDateTime endPeriod = LocalDateTime.of(2018, 9, 20, 23, 0);
        LocalDateTime datePeriodOut = LocalDateTime.of(2018, 9, 20, 23, 59, 59);

        Check check1 = new Check();
        check1.setCheckId(100l);
        check1.setAmount(BigDecimal.valueOf(100.2));
        check1.setOperationDate(startPeriod);

        Check check2 = new Check();
        check2.setCheckId(200l);
        check2.setAmount(BigDecimal.valueOf(200.2));
        check2.setOperationDate(startPeriod);

        Check check3 = new Check();
        check3.setCheckId(300l);
        check3.setAmount(BigDecimal.valueOf(300.2));
        check3.setOperationDate(endPeriod);

        Check check4 = new Check();
        check4.setCheckId(400l);
        check4.setAmount(BigDecimal.valueOf(9999999999.99));
        check4.setOperationDate(datePeriodOut);

        checkStatExpect = new CheckStat();
        checkStatExpect.setCheckCount(3L);
        checkStatExpect.setAvgAmount(BigDecimal.valueOf(200.20));
        checkStatExpect.setSumAmount(BigDecimal.valueOf(600.60));

        dateRange = new DateRange();
        dateRange.setStartDate(startPeriod);
        dateRange.setEndDate(endPeriod);

        testData = Arrays.asList(check1, check2, check3, check4);
    }

    @Test
    public void testCheckStaItByPeriod() {
        for (Check check : testData) {
            ResponseEntity<Check> responseCheck = restTemplate.postForEntity("/check", check, Check.class);
            Assert.assertEquals(responseCheck.getStatusCodeValue(), 200);
        }

        ResponseEntity<CheckStat> responseCheckStat = restTemplate.postForEntity("/stat/checkByDateRange", dateRange, CheckStat.class);

        CheckStat checkStatActual = responseCheckStat.getBody();

        Assert.assertEquals( checkStatActual.getCheckCount(), checkStatExpect.getCheckCount());
        Assert.assertEquals(
                checkStatActual.getAvgAmount().setScale(2,2),
                checkStatExpect.getAvgAmount().setScale(2,2));
        Assert.assertEquals(
                checkStatActual.getSumAmount().setScale(2, 2),
                checkStatExpect.getSumAmount().setScale(2,2));

    }

    //TODO testCheckByBadPerid
}