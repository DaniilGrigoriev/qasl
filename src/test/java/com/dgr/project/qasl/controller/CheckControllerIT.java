package com.dgr.project.qasl.controller;

import com.dgr.project.qasl.model.Check;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by user on 20.09.2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CheckControllerIT {
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    public CheckControllerIT() {
    }

    public TestRestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(TestRestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Before
    public void before() {
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }

    @Test
    public void testSaveAndGetCheck() {
        Check checkExpect = new Check();

        checkExpect.setCheckId(11L);
        checkExpect.setAmount(BigDecimal.valueOf(123.12));
        checkExpect.setOperationDate(LocalDateTime.now());

        restTemplate.postForEntity("/check", checkExpect, Check.class);

        Map<String, Object> paramForGetRequest= new HashMap<>();
        paramForGetRequest.put("id", checkExpect.getCheckId());
        ResponseEntity<Check> responseCheck = restTemplate.getForEntity("/check/{id}", Check.class, paramForGetRequest);

        Check checkActual = responseCheck.getBody();

        Assert.assertEquals(checkActual.getCheckId(), checkExpect.getCheckId());
        Assert.assertEquals(checkActual.getAmount(), checkExpect.getAmount());
        Assert.assertEquals(checkActual.getOperationDate(), checkExpect.getOperationDate());
    }

    @Test
    public void testSaveCheckValidation(){
        Check checkWithNullAmount = new Check();

        checkWithNullAmount.setCheckId(22L);
        checkWithNullAmount.setAmount(null);
        checkWithNullAmount.setOperationDate(LocalDateTime.now());

        Check checkWithNullDate = new Check();

        checkWithNullDate.setCheckId(22L);
        checkWithNullDate.setAmount(BigDecimal.valueOf(123.33));
        checkWithNullDate.setOperationDate(null);

        ResponseEntity<Check> responseCheckWithNullAmount = restTemplate.postForEntity("/check", checkWithNullAmount, Check.class);
        Assert.assertEquals(responseCheckWithNullAmount.getStatusCodeValue(), 400);

        ResponseEntity<Check> responseCheckWithNullDate = restTemplate.postForEntity("/check", checkWithNullDate, Check.class);
        Assert.assertEquals(responseCheckWithNullDate.getStatusCodeValue(), 400);
    }
}